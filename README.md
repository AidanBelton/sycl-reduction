## Reduction implementations

#### USM

- reduction-usm-01 : manual reduction implementation using ND-range parallel-for and barrier
- reduction-usm-02 : reduction expressed as an ND-range data-parallel kernel using the reduction library
- reduction-usm-03 : reduction using ND-range parallel-for, a work-group reduce function, and an atomic operation
- reduction-usm-04 : reduction using a basic parallel-for and an atomic operation for every work-item

#### Buffer

- reduction-buffer-01 : manual reduction implementation using ND-range parallel-for and barrier
- reduction-buffer-02 : reduction expressed as an ND-range data-parallel kernel using the reduction library
- reduction-buffer-03 : reduction using ND-range parallel-for, a work-group reduce function, and an atomic operation

## Run

```console
make -f Makefile.usm-01 ARCH=V100; srun reduction-usm-01.x <vector size> <workgroup size>
make -f Makefile.usm-01 ARCH=V100; srun reduction-usm-01.x 1048576 1024
make -f Makefile.usm-01 ARCH=A100; srun reduction-usm-01.x 1048576 1024
```

## Reference
#### Book
Reinders, J., Ashbaugh, B., Brodman, J., Kinsner, M., Pennycook, J., and Tian, X. Data Parallel C++: Mastering DPC++ for Programming of Heterogeneous Systems using C++ and SYCL, Chapter 14, Apress, 2020. https://www.apress.com/gp/book/9781484255735
#### Examples
https://github.com/Apress/data-parallel-CPP/tree/main/samples

