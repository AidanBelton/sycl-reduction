#include <CL/sycl.hpp>
#include <iostream>
#include <array>
#include <chrono>

using namespace cl::sycl;
//using namespace sycl::ONEAPI;

#include <cassert>
template<class T>
bool almost_equal(T x, T y, double tol)
{
    return std::abs( x - y ) < tol ;
}

typedef std::chrono::system_clock Clock;

#ifndef ITERATIONS
#  define ITERATIONS 1
#endif

static unsigned int verbose=1;
static size_t warmups=1;

// SYCL lambda function kernel names
class kernel_reduction;

int main(int argc, char **argv)
{
 
  if(argc < 2){
    std::cerr << "Usage <vector size> <workgroup size>" << std::endl;
    exit(1);
  }

  size_t N = atoi(argv[1]);
  size_t workgroup_size = atoi(argv[2]);
  size_t num_groups = N/workgroup_size; 
  size_t iterations = ITERATIONS;

  //using memory_order = sycl::ONEAPI::memory_order;
  //using memory_scope = sycl::ONEAPI::memory_scope;

  default_selector selector;
  queue Q(selector);

  if (verbose > 0) {
	  // Print device information
	  std::cout << "Getting device info: ";
	  std::cout << "Device " << selector.select_device().get_info<info::device::name>() \
	            << ": Driver " << selector.select_device().get_info<info::device::driver_version>() << std::endl;
	  std::cout << "max compute units = " 
		    << Q.get_device().get_info<info::device::max_compute_units>() << "\n";
	  std::cout << "max workgroup size = " 
		    << Q.get_device().get_info<info::device::max_work_group_size>() << "\n";
	  std::cout << std::flush;
  }

  double *input = (double*)malloc(sizeof(double)*N);
  for (int i = 0; i < N; i++) { input[i] = i; }

  double chksum = 0.;
  for(int i = 0; i < N; ++i) {
    chksum += input[i]; 
  }

  buffer buf_in{input,range{N}};
  buffer<double> buf_sum{range{1}};

  range global_range{N};
  range local_range{workgroup_size}; 

  if (verbose > 0) {
    std::cout << "Setting number of work items to " << N << std::endl;
    std::cout << "Setting number of work groups to " << num_groups << std::endl;
    std::cout << "Setting workgroup size to " <<  N/num_groups << std::endl;
  }

  auto tstart = Clock::now();
  for (int iters=0; iters<iterations+warmups; ++iters) {
     if (iters == warmups) {
        Q.wait();
        tstart = Clock::now();
     } 

     Q.submit([&](handler &h) {
        accessor d_sum{buf_sum, h, write_only};  
        h.single_task<class initialize>([=] () {
           d_sum[0] = 0.;
        });
     });

     Q.submit([&](handler &h) {
        accessor d_in{buf_in, h, read_only};  
        accessor d_sum{buf_sum, h, read_write};  
        h.parallel_for<class kernel_reduction>(nd_range{global_range, local_range}, [=] (nd_item<1> id) {
           int global_id = id.get_global_id(0);
           int group_sum = sycl::ONEAPI::reduce(id.get_group(), d_in[global_id], sycl::ONEAPI::plus<>());
           if (id.get_local_id(0) == 0) {
	      sycl::ONEAPI::atomic_ref<double, sycl::ONEAPI::memory_order::relaxed, sycl::ONEAPI::memory_scope::system, access::address_space::global_space>(d_sum[0]) += group_sum;
           }
        });
     });
     Q.wait();
  }
  double ttotal = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-tstart).count();

  host_accessor h_sum{buf_sum, read_only};

  if (verbose > 0)
    std::cout << "Total execution time = " << ttotal / 1.0e6 << " secs" << std::endl;
 
  assert(almost_equal<double>(h_sum[0], chksum, 2E-6));

  free(input);

  return 0;
}

